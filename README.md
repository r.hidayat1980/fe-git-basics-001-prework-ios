

## GIT Basics ~ 1hr 27min

Watch the videos below if you are unfamiliar with GIT. We will be using GIT to access course materials and to share and collaborate on project code. These videos are also available as a single continuous [playlist from the link here](https://www.youtube.com/watch?v=yeank75sObU&list=PLj148bJp5wiwdhbnxCrxZzbDcp4l7KDpX).

<iframe width="640" height="360" src="//www.youtube.com/embed/yeank75sObU?list=PLj148bJp5wiwdhbnxCrxZzbDcp4l7KDpX" frameborder="0" allowfullscreen></iframe>

[GIT Cheatsheet](http://www.git-tower.com/blog/content/posts/54-git-cheat-sheet/git-cheat-sheet-large01.png)

[GIT Best Practices](http://www.git-tower.com/blog/content/posts/54-git-cheat-sheet/git-cheat-sheet-large02.png)

<p data-visibility='hidden'>View <a href='https://learn.co/lessons/fe-git-basics' title='GIT Basics ~ 1hr 27min'>GIT Basics ~ 1hr 27min</a> on Learn.co and start learning to code for free.</p>
